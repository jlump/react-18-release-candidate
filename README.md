# Getting Started with react 18 (release candidate version)

https://reactjs.org/blog/2021/06/08/the-plan-for-react-18.html

# new feature
react 18 contain new Root API "createRoot" which replacing "render" (legacy root API) that allow to use new features
- You can use React 18 with legacy render to keep the old behavior

## automatic batching 
    React 17 batches only inside event handlers.
    React 18 with createRoot batches even outside event handlers
- you can use ReactDOM.flushSync() to opt out of batching
    ```
    flushSync(() => {
        setCounter(c => c + 1);
    });
    ```
  
for testing, you can switch to render AppBatching instead of App in index.js file
  
## New APIs

### startTransition
    
import as

`import { startTransition } from 'react'`

or use as react hook (allow using isPending)

`const [isPending, startTransition] = useTransition();`

With startTransition, allow developer to separate immediate UI renders and non-urgent UI renders action by putting non-urgent action inside the startTransition

    startTransition(() => {
        nonUrgentAction(value)
    })

By using as useTransition hook, it also provides isPending variable to indicate the status of the transition.

It can be interrupted by more pressing update action, and React will just throw out the unfinished, now-outdated rendering work and jump right to the new stuff

### useDeferredValue

useDeferredValue allows developer to select specific parts of UI and intentionally defer updating them, so they don’t slow down other parts

So 2 features mentioned above allow you to prevent non-urgent action which clause page slow down or lag
